import { createIcons, LogIn, SkipBack} from 'lucide';

export function createIcons(){
  createIcons({
      icons: {
        LogIn,
        SkipBack
      },
      attrs: {
        class: ['icon'],
        'stroke-width': 0.5,
        stroke: '#000',
        color: '#f0f',
      },
  });
};
